# solarized dark for qt

### for kde users [this](https://github.com/ret2src/kde-plasma-solarized) exists, you may as well use it

### this is intended to be used outside kde (using `qt5ct`/`qt6ct`), see (3)

copy `solarzed-dark-qt.conf` from this repo into
`$XDG_CONFIG_HOME/qt5ct/colors/`, copy `solarzed-dark-qt.qss` into 
`$XDG_CONFIG_HOME/qt5ct/qss/` and select the corresponding colorscheme and style
sheet in `qt5ct`


if you use dolphin (maybe something else too, idk) ouside kde, it's gonna have
its main area background fucked up. put the contents of `dolphin-bg-fix` from
this repo into `~/.config/kdeglobals`. see (2)


### credits and references:

1. mostly taken from [here](https://gitlab.com/GasparVardanyan/themes)
2. https://wiki.archlinux.org/title/Dolphin#Mismatched_folder_view_background_colors
3. https://wiki.archlinux.org/title/Qt#Configuration_of_Qt_5_applications_under_environments_other_than_KDE_Plasma

